#Simple Selenium Landing Tester

## What is it ?

An attempt to automate IU testing in Selenium with simple landing pages



## When to use  this project ?

- **Situation** #1 :
  - You have a landing page that consists of either:
    - A single HTML file that uses a **<form>**
    - Several HTML files in a single directory that fills data in a **<form>**
  - You need to test:
    - Changes in your input elements **style** in response to user interaction
    - Changes in your page according to specific script-called HTTP Requests results

